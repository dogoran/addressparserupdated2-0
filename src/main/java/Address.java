import java.util.ArrayList;

/**
 * Created by dogor on 08.04.2017.
 */
public class Address {

    private String country;
    private String region;
    private String city;
    private String street;
    private String houseId;
    private String houseSubId;
    private String doorId;



    Address(String country, String region, String city,
            String street, String houseId, String houseSubId, String doorId){
        this.country = country;
        this.region = region;
        this.city = city;
        this.street = street;
        this.houseId = houseId;
        this.houseSubId = houseSubId;
        this.doorId = doorId;
    }


    Address(){}



    public String getCountry(){
        return this.country;
    }

    public String getRegion(){
        return this.region;
    }

    public String getCity(){
        return this.city;
    }

    public String getHouseId(){
        return this.houseId;
    }

    public String getHouseSubId(){
        return this.houseSubId;
    }

    public String getDoorId(){
        return this.doorId;
    }

    public String getStreet(){
        return this.street;
    }

    @Override

    public String toString(){
        return  "Country: "  + getCountry() + "  Region: " + getRegion()
                + "  City: " + getCity() + "  HouseId: " + getHouseId()
                + "  HouseSubId: " + getHouseSubId() + "  DoorId: " +  getDoorId() + "\n";
    }

}