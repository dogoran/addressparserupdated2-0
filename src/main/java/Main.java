import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dogor on 08.04.2017.
 */
public class Main {

    public static int lines_count;

    public static void main(String[] args) throws IOException {
        lines_count = 4;
        List<Address> addres;

        ParserQ pars = new ParserQ();
        addres = pars.parse(Paths.get("src\\main\\resources\\text.txt"));
        showList(addres);
    }

    public static void showList(List<Address> addr){
        for(Address elements : addr){
            System.out.println(elements);
        }
    }
}
