import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by dogor on 08.04.2017.
 */
public class ParserQ {

    public ParserQ(){}


    public List<Address> parse(Path path) throws IOException {

        ArrayList<String> resullt = new ArrayList();
        List<Address>  addressList = new ArrayList<>();
        List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);

        for (String line: lines) {
            StringTokenizer st = new StringTokenizer(line.toString(), ".,;-");

            while(st.hasMoreTokens()){
                resullt.add(st.nextToken());
            }

            addressList.add(new Address(resullt.get(0),resullt.get(1),resullt.get(2),
                    resullt.get(3),resullt.get(4),resullt.get(5),resullt.get(6)));
            resullt.clear();
        }

        return addressList;
    }


}