import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by dogor on 08.04.2017.
 */
public class AddressTest {

    private static int lines_count;
    List<Address> addres;


    @Before
    public void init() throws IOException {
        lines_count = 4;

        ParserQ pars = new ParserQ();
        addres = pars.parse(Paths.get("src\\main\\resources\\text.txt"));
        showList(addres);
    }

    public static void showList(List<Address> addr){

        for(Address elements : addr){
            System.out.println(elements);
        }

    }


    @Test

    public void countryTest(){
        assertEquals(addres.get(0).getCountry(),"НеСтрана");
        assertEquals(addres.get(1).getCountry(),"USA");
        assertEquals(addres.get(2).getCountry(),"Africa");
        assertEquals(addres.get(3).getCountry(),"UA");
    }


    @Test
    public void regionTest(){
        assertEquals(addres.get(0).getRegion(),"Чубака");
        assertEquals(addres.get(1).getRegion(),"NY");
        assertEquals(addres.get(2).getRegion(),"Tanzania");
        assertEquals(addres.get(3).getRegion(),"DONETSK");
    }

    @Test
    public void cityTest(){
        assertEquals(addres.get(0).getCity(),"Пушкина");
        assertEquals(addres.get(1).getCity(),"WS");
        assertEquals(addres.get(2).getCity(),"KS");
        assertEquals(addres.get(3).getCity(),"DONETSK");
    }

    @Test
    public void houseIdTest(){
        assertEquals(addres.get(0).getHouseId(),"56");
        assertEquals(addres.get(1).getHouseId(),"16");
        assertEquals(addres.get(2).getHouseId(),"4");
        assertEquals(addres.get(3).getHouseId(),"2");
    }

    @Test
    public void houseSubIdTest(){
        assertEquals(addres.get(0).getHouseSubId(),"123");
        assertEquals(addres.get(1).getHouseSubId(),"12");
        assertEquals(addres.get(2).getHouseSubId(),"122");
        assertEquals(addres.get(3).getHouseSubId(),"14");
    }

    @Test
    public void doorIdTest(){
        assertEquals(addres.get(0).getDoorId(),"312");
        assertEquals(addres.get(1).getDoorId(),"2");
        assertEquals(addres.get(2).getDoorId(),"12");
        assertEquals(addres.get(3).getDoorId(),"2");
    }
}
